package Game;

import java.util.ArrayList;

public class Position
{
	public int col;
	public int row;
	public int movesLeft = 0;
	public Position parent;

	public Position()
	{

	}

	public Position(int row, int col)
	{
		this.col = col;
		this.row = row;
	}

	public Position(int row, int col, int movesLeft)
	{
		this.col = col;
		this.row = row;
		this.movesLeft = movesLeft;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof Position)
		{
			Position pos = (Position) obj;
			return pos.col == this.col && pos.row == this.row;
		}

		return super.equals(obj);
	}

	@Override
	public int hashCode()
	{
		int hash = 2 + row / 3 + col * 23 / 7;
		return hash;
	}

	@Override
	public String toString()
	{
		return "Row: " + row + " Col: " + col;
	}

	public static int getDistance(Position unitPos, Position targetPos)
	{
		int distance = 0;

		distance += Math.abs(unitPos.col - targetPos.col);
		distance += Math.abs(unitPos.row - targetPos.row);
		return distance;
	}

	// if (radius == 0)
	public static ArrayList<Position> getAllPositionsXSquaresAway(
			Position origin, int radius)
	{
		if (radius == 0)
		{
			return new ArrayList<Position>();
		}
		ArrayList<Position> pos = new ArrayList<Position>();
		for (int row = origin.row - radius; row <= origin.row + radius; row++)
		{
			for (int col = origin.col - radius; col <= origin.col + radius; col++)
			{
				// Off board
				if (row < 0 || row >= Constants.height || col < 0
						|| col >= Constants.width)
				{
					continue;
				}
				pos.add(new Position(row, col));
			}
		}
		return pos;
	}

	/**
	 * returns all positions inside two ranges does not remove origin.
	 * 
	 * @param origin
	 * @param min
	 * @param max
	 * @return
	 */
	public static ArrayList<Position> getAllPositionsUpTo(Position origin,
			int min, int max)
	{

		ArrayList<Position> pos = new ArrayList<Position>();
		for (int row = origin.row - max; row <= origin.row + max; row++)
		{
			for (int col = origin.col - max; col <= origin.col + max; col++)
			{
				// Off board
				if (row < 0 || row >= Constants.height || col < 0
						|| col >= Constants.width)
				{
					continue;
				}
				Position des = new Position(row, col);
				int distance = Position.getDistance(origin, des);
				if (distance >= min && distance <= max)
					pos.add(des);
			}
		}
		// pos.remove(origin);
		return pos;
	}

	public ArrayList<Position> getNeighbours()
	{

		ArrayList<Position> pos = new ArrayList<Position>();
		if (col + 1 < Constants.width)
		{
			pos.add(new Position(row, col + 1));
		}
		// Left
		if (col - 1 >= 0)
		{
			pos.add(new Position(row, col - 1));
		}
		// UP

		if (row + 1 < Constants.height)
		{
			pos.add(new Position(row + 1, col));
		}
		// DOWN
		if (row - 1 >= 0)
		{
			pos.add(new Position(row - 1, col));
		}
		return pos;

	}

	public enum Direction
	{
		LEFT,RIGHT,DOWN,UP,ERROR;
	}
	public static Direction getDirection(Position from, Position to)
	{
		if (from.row != to.row)
		{
			if (from.row + 1== to.row)
			{
				return Direction.UP;
			}
			else if (from.row - 1 == to.row)
			{
				return Direction.DOWN;
			}
		}
		else if (from.col != to.col)
		{
			if (from.col + 1== to.col)
			{
				return Direction.RIGHT;
			}
			else if (from.col - 1 == to.col)
			{
				return Direction.LEFT;
			}
		}
		return Direction.ERROR;
	}
}
