package Game;

import Game.Units.UnitFactory;

public class Begin
{
	public static void main(String args[])
	{
		UnitFactory.initUnitsFromFile();
		Board b = Board.getInstance();
		b.newGame();
		b.printBoard();

	}
}
