package Game;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;

import Game.AI.Computer;
import Game.AI.InfluenceComputerV2;
import Game.Tiles.Building;
import Game.Tiles.Headquarters;
import Game.Tiles.Tile;
import Game.Tiles.TileFactory;
import Game.Units.Unit;
import Game.Units.UnitFactory;
import Game.Units.Weapon.AttackObject;
import Game.Units.Weapon.Weapon;

public class Board
{

	public static final String UPDATE_REASON_END_TURN = "TURN END";
	public static final String UPDATE_REASON_MOVE = "MOVE";
	public static final String UPDATE_REASON_REPAIR = "REPAIR";
	public static final String UPDATE_REASON_KILL = "KILL";
	private static Board INSTANCE;
	private static Tile board[][];
	int turn = Constants.TEAM_BLUE;
	int day = 1;
	ArrayList<ArrayList<Building>> allBuildings = new ArrayList<ArrayList<Building>>();
	Player players[] = new Player[2];
	ArrayList<ArrayList<Unit>> allUnits = new ArrayList<ArrayList<Unit>>();
	Set<Observer> observers = new HashSet<Observer>();
	private static Building[] hqs = new Building[2];
	private boolean gameOver = true;

	// ArrayList<Building> buildings = new ArrayList<Building>();
	private Board()
	{

	}

	public static Board getInstance()
	{
		if (INSTANCE == null)
		{
			INSTANCE = new Board();
		}

		return INSTANCE;
	}

	public Tile[][] getBoard()
	{
		return board;
	}

	public void newGame()
	{
		gameOver = false;
		players[0] = new InfluenceComputerV2(Constants.TEAM_BLUE);
		players[1] = new Player(Constants.TEAM_RED);
		// players[0] = new InfluenceComputer(Constants.TEAM_BLUE);
		// players[1] = new InfluenceComputerV2(Constants.TEAM_RED);
		turn = 0;
		allBuildings.add(new ArrayList<Building>());
		allBuildings.add(new ArrayList<Building>());
		allBuildings.add(new ArrayList<Building>());
		allUnits.add(new ArrayList<Unit>());
		allUnits.add(new ArrayList<Unit>());
		allUnits.add(new ArrayList<Unit>());

		initBoardFromFile();
		beginGame();
	}

	public void beginGame()
	{
		if (players[0] instanceof Computer)
		{
			Thread thread = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					((Computer) players[0]).makeMove();
				}

			});
			thread.start();
		
		}
	}

	private void initBoardFromFile()
	{
		board = new Tile[Constants.width][Constants.height];
		// File file = new File(Constants.boardFile);
		// System.out.println(new File(".").getAbsolutePath());
		// InputStream stream = getClass().getResourceAsStream("board.txt");
		InputStream stream2 = getClass().getClassLoader().getResourceAsStream(
				Constants.boardFile);

		// scanner = new Scanner(stream); todo same thing try it out dude it may
		// not work though lolollolo.
		Scanner scanner;

		try
		{
			scanner = new Scanner(stream2);
			int row = 0;

			// Tile 1 = type
			// 2 = team if building
			// 3 = unit
			// 4 = unit team
			while (scanner.hasNextLine())
			{
				String boardRow = scanner.nextLine();
				String tileStrs[] = boardRow.split(" ");
				int column = 0;
				for (String tileStr : tileStrs)
				{
					int currentChar = 0;
					Tile tile = TileFactory.tileFromChar(
							tileStr.charAt(currentChar), row, column);
					currentChar++;
					if (tile.isBuilding())
					{
						Building building = (Building) tile;
						building.setTeam(tileStr.charAt(currentChar) - '0');
						currentChar++;
						board[row][column] = building;
						allBuildings.get(building.getTeam()).add(building);
						if (building instanceof Headquarters)
						{
							hqs[building.getTeam()] = building;
						}
					}
					else
					{
						board[row][column] = tile;
					}

					// TODO init with untis on board.
					// Has unit
					/*
					if (tileStr.length() > currentChar + 1)
					{
						Unit unit = UnitFactory.createUnit(
								tileStr.charAt(currentChar), row, column,
								tileStr.charAt(currentChar + 1) - '0');

						currentChar += 2;

					}
					 */

					column++;
				}
				row++;
			}
			scanner.close();
		}
		// catch (FileNotFoundException e)
		// {
		// e.printStackTrace();
		// }
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void printBoard()
	{
		for (int row = 0; row < Constants.width; row++)
		{
			for (int col = 0; col < Constants.height; col++)
			{
				System.out.print(board[row][col].toString() + " ");
			}
			System.out.println();
		}
	}

	// --------------------------- GAME RELATED------------------------------

	public void endTurn()
	{
		if (gameOver)
		{
			return;
		}
		turn ^= 1;
		if (turn == Constants.TEAM_BLUE)
		{
			day++;
		}
		ArrayList<Position> updates = restoreBuildings();
		generateGold();

		ArrayList<Unit> units = allUnits.get(turn);
		ArrayList<Unit> deadUnits = new ArrayList<Unit>();
		ArrayList<Unit> repairedUnits = new ArrayList<Unit>();
		for (Unit unit : units)
		{
			unit.renew();

			unit.upkeepGas();

			Tile tile = getTile(unit);
			if (tile.isBuilding() && unit != null && unit.getTeam() == turn
					&& unit.getTeam() == ((Building) tile).getTeam())
			{
				if (unit.getHealth() < unit.getMaxHealth())
				{

					int cost = UnitFactory.getRepairCost(unit);
					if (players[turn].getGold() > cost)
					{
						players[turn].pay(cost);
						unit.repair();
						repairedUnits.add(unit);
					}
				}
				unit.resupply();
			}

			if (unit.isDead())
			{
				tile.setUnit(null);
				deadUnits.add(unit);
			}
			updates.add(unit.getPosition());
		}

		units.removeAll(deadUnits);

		Set<Position> set = new HashSet<Position>(updates);
		ArrayList<Position> list = new ArrayList<Position>(set);

		notifyObservers(list);
		notifyObservers(getCurrentPlayer());

		// TODO
		// upkeep
		// resupply units
		// KILL THOSE WHO CANT PAY UPKEEP

		notifyObservers(getCurrentPlayer(), Board.UPDATE_REASON_END_TURN);
		notifyObservers(repairedUnits, Board.UPDATE_REASON_REPAIR);
		if (getCurrentPlayer() instanceof Computer)
		{
			Thread thread = new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					((Computer) getCurrentPlayer()).makeMove();
				}

			});
			thread.start();
		}
	}

	public ArrayList<Position> getPossibleMoves(Tile t)
	{
		ArrayList<Position> possibleMoves = new ArrayList<Position>();
		if (t.hasUnit())
		{
			getPossibleMoves(t.getUnit(), t.getUnit().getNumMoves(),
					t.getPosition(), possibleMoves);
			possibleMoves.remove(t.getPosition());
			return possibleMoves;
		}
		else
		{
			return possibleMoves;
		}
	}

	public ArrayList<Position> getTargets(Tile t, boolean isPrimary)
	{
		Unit unit = t.getUnit();
		if (unit == null)
		{
			return new ArrayList<Position>();
		}

		Weapon weapon;

		if (isPrimary)
		{
			weapon = unit.getPrimaryWeapon();
		}
		else
		{
			weapon = unit.getSecondaryWeapon();
		}

		if (weapon == null || !weapon.hasAmmo())
		{
			return new ArrayList<Position>();
		}

		int maxRange = weapon.maxRange;
		int minRange = weapon.minRange;
		Position thisPos = new Position(t.row, t.col, maxRange);
		ArrayList<Position> targets = getTargets(unit, thisPos, minRange,
				maxRange, weapon.radius);
		// getTargets(unit, maxRange, thisPos, positionHolder, targets,
		// isPrimary);
		filterMinRange(thisPos, targets, minRange);
		return targets;
	}

	public ArrayList<Position> getSimulatedTargets(Unit unit, Tile origin,
			boolean isPrimary)
	{
		if (unit == null)
		{
			return new ArrayList<Position>();
		}

		Weapon weapon;

		if (isPrimary)
		{
			weapon = unit.getPrimaryWeapon();
		}
		else
		{
			weapon = unit.getSecondaryWeapon();
		}

		if (weapon == null || !weapon.hasAmmo())
		{
			return new ArrayList<Position>();
		}

		int maxRange = weapon.maxRange;
		int minRange = weapon.minRange;

		Position thisPos = new Position(origin.row, origin.col, maxRange);
		ArrayList<Position> targets = getTargets(unit, thisPos, minRange,
				maxRange, weapon.radius);
		// getTargets(unit, maxRange, thisPos, positionHolder, targets,
		// isPrimary);
		filterMinRange(thisPos, targets, minRange);
		return targets;
	}

	private void filterMinRange(Position unitPos, ArrayList<Position> targets,
			int minRange)
	{

		ArrayList<Position> outOfRange = new ArrayList<Position>();
		for (Position targetPos : targets)
		{
			if (minRange > Position.getDistance(unitPos, targetPos))
			{
				outOfRange.add(targetPos);
			}
		}
		targets.removeAll(outOfRange);
	}

	public void attack(Tile atker, Tile defender, boolean isPrimary)
	{
		ArrayList<Position> changedPos = new ArrayList<Position>();
		changedPos.add(atker.getPosition());
		AttackObject atkObj = atker.attack(defender, isPrimary);
		Unit atkingUnit = atker.getUnit();
		ArrayList<Unit> deadUnits = new ArrayList<Unit>();
		if (atkObj.error == null)
		{
	

			// Splash
			for (int row = defender.row - atkObj.radius; row <= defender.row
					+ atkObj.radius; row++)
			{
				for (int col = defender.col - atkObj.radius; col <= defender.col
						+ atkObj.radius; col++)
				{

					// Off board
					if (row < 0 || row >= Constants.height || col < 0
							|| col >= Constants.width)
					{
						continue;
					}
					else
					{
						Tile tile = board[row][col];
						Position thisPos = new Position(row, col);
						if (Position.getDistance(defender.getPosition(),
								thisPos) <= atkObj.radius)
						{
							// TODO fix luck, need to store base or else.
							// atkObj.damage += randInt(Constants.LUCK_MIN,
							// Constants.LUCK_MAX);
							// Splash actually hit a unit!
							if (tile.hasUnit())
							{
								Unit unit = tile.getUnit();
								unit.takeDamage(atkObj, atkingUnit);

								if (unit.isDead())
								{
									deadUnits.add(unit);

									atkingUnit.addKill(unit);
									tile.setUnit(null);
								}
								else
								{
									if (unit.canRetaliate(atkingUnit))
									{
										AttackObject counteratk = unit
												.retaliate(atker);

										atkingUnit.takeDamage(counteratk, unit);

										if (atkingUnit.isDead())
										{
											deadUnits.add(atkingUnit);
											unit.addKill(atkingUnit);
											atker.setUnit(null);
										}
									}
								}

								changedPos.add(tile.getPosition());
							}
						}
					}
				}
			}

			for (Unit unit : deadUnits)
			{
				ArrayList<Unit> teamUnitList = allUnits.get(unit.getTeam());
				teamUnitList.remove(unit);
			}
		}
		else
		{
			System.out.println(atkObj.error);
		}
		notifyObservers(deadUnits, UPDATE_REASON_KILL);
		notifyObservers(changedPos);
		// Do splash.
	}

	// TODO ERROR CHECK THIS, NAH.
	public void move(Tile src, Tile des)
	{
		if (src.equals(des))
		{
			return;
		}
	

		ArrayList<Position> positions = new ArrayList<Position>(); // Animation
		positions.add(src.getPosition());
		positions.add(des.getPosition());
		ArrayList<Position> animatePositions = findPath(src, des);
		
		Unit unit = src.getUnit();
		unit.move();
		Position pos = des.getPosition();
		unit.col = pos.col;
		unit.row = pos.row;
		unit.payGas(Position.getDistance(src.getPosition(), des.getPosition()));
		des.setUnit(unit);
		src.setUnit(null);
		
		notifyObservers(animatePositions, UPDATE_REASON_MOVE);
		notifyObservers(positions);
		notifyObservers(unit);
		System.out.println("Unit Moved");
	}

	private ArrayList<Position> findPath(Tile src, Tile des)
	{
		// ArrayList<Position> positions = new ArrayList<Position>();
		// positions.add(src.getPosition());

		Unit unit = src.getUnit();
		PriorityQueue<Position> frontier = new PriorityQueue<Position>(10,
				new Comparator<Position>()
				{

					@Override
					public int compare(Position arg0, Position arg1)
					{
						int m1 = arg0.movesLeft;
						int m2 = arg1.movesLeft;
						if (m1 > m2)
						{
							return 1;
						}
						else if (m1 < m2)
						{
							return -1;
						}
						return 0;
					}
				});

		HashMap<Position, Integer> costMap = new HashMap<Position, Integer>();

		// ArrayList<Position> closed = new ArrayList<Position>();
		Position start = src.getPosition();
		start.parent = null;
		frontier.add(start);
		costMap.put(start, 0);
		start.movesLeft = 0;
		Position goal = des.getPosition();
		Position current = null;
		while (!frontier.isEmpty())
		{
			current = frontier.remove();
			if (current.equals(goal))
			{
				break;
			}
			for (Position next : current.getNeighbours())
			{
				int cost = 0;
				if (getTile(next).getUnit() != null)
				{
					if (!isFriendly(unit, getTile(next).getUnit()))
					{
						cost = 999;
					}
					else
					{
						cost = getTile(next).getMovementCost(unit);
					}
				}
				else
				{
					cost = getTile(next).getMovementCost(unit);
				}
				int new_cost = cost + costMap.get(current);
				Integer prevCost = costMap.get(next);
				if (prevCost == null || new_cost < prevCost)
				{
					costMap.put(next, new_cost);
					next.movesLeft = new_cost + heuristic(next, goal);
					frontier.add(next);
					next.parent = current;
				}
			}
			// closed.add(current);
		}
		ArrayList<Position> path = new ArrayList<Position>();
		path.add(current);
		while (!current.equals(start))
		{
			current = current.parent;
			path.add(current);
		}
		Collections.reverse(path);
		return path;
	}

	private boolean isFriendly(Unit unit, Unit unit2)
	{
		if (unit.getTeam() == unit2.getTeam())
			return true;
		return false;
	}

	private int heuristic(Position src, Position des)
	{
		return Math.abs(src.row - des.row) + Math.abs(src.col - des.col);
	}

	/**
	 * Returns the changed positions to update
	 */
	private ArrayList<Position> restoreBuildings()
	{
		ArrayList<Position> updates = new ArrayList<Position>();
		for (ArrayList<Building> teamsBuildings : allBuildings)
		{
			for (Building building : teamsBuildings)
			{
				// TODO maybe change later, only restore health if no unit on
				// top.
				if (!building.hasUnit()
						&& building.getHealth() != Constants.BUILDING_MAX_HEALTH)
				{
					updates.add(building.getPosition());
					building.restoreHealth();
				}
			}
		}
		return updates;

	}

	private void generateGold()
	{
		ArrayList<Building> teamsBuildings = allBuildings.get(turn);
		int amount = teamsBuildings.size() * Constants.GOLD_PER_BUILDING;
		players[turn].addGenerationGold(amount);
	}

	private void getPossibleMoves(Unit unit, int movesLeft, Position current,
			ArrayList<Position> prevMoves)
	{
		int col = current.col;
		int row = current.row;

		// RIGHT
		if (col + 1 < Constants.width)
		{
			Tile tile = board[row][col + 1];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col + 1;
				pos.row = row;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}

		// LEFT
		if (col - 1 >= 0)
		{
			Tile tile = board[row][col - 1];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col - 1;
				pos.row = row;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}

		// UP

		if (row + 1 < Constants.height)
		{
			Tile tile = board[row + 1][col];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col;
				pos.row = row + 1;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}

		// DOWN
		if (row - 1 >= 0)
		{
			Tile tile = board[row - 1][col];

			int cost = tile.getMovementCost(unit);
			if (movesLeft - cost >= 0)
			{
				Position pos = new Position();
				pos.col = col;
				pos.row = row - 1;
				int newMovesLeft = movesLeft - cost;
				pos.movesLeft = newMovesLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
					}
				}
				else
				{
					if (tile.getUnit() == null)
					{
						prevMoves.add(pos);
						getPossibleMoves(unit, newMovesLeft, pos, prevMoves);

					}
					else
					{
						if (tile.getUnit().getTeam() == unit.getTeam())
						{
							getPossibleMoves(unit, newMovesLeft, pos, prevMoves);
						}
					}
				}
			}
		}
	}

	private ArrayList<Position> getTargets(Unit unit, Position origin,
			int minRange, int maxRange, int radius)
	{
		ArrayList<Position> positions = Position.getAllPositionsUpTo(origin,
				minRange, maxRange);
		if (radius == 0)
		{
			Iterator<Position> iter = positions.iterator();
			while (iter.hasNext())
			{
				Unit target = getTile(iter.next()).getUnit();
				if (target == null || unit.getTeam() == target.getTeam())
				{
					iter.remove();
				}
			}
		}
		return positions;
	}

	/*
	private void getTargets(Unit unit, int rangeLeft, Position current,
			ArrayList<Position> prevMoves, ArrayList<Position> targets,
			boolean isPrimary)
	{
		int col = current.col;
		int row = current.row;

		// RIGHT
		if (col + 1 < Constants.width)
		{
			if (rangeLeft - 1 >= 0)
			{
				Position pos = new Position();
				pos.col = col + 1;
				pos.row = row;
				int newRangeLeft = rangeLeft - 1;
				pos.movesLeft = newRangeLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getTargets(unit, newRangeLeft, pos, prevMoves, targets,
								isPrimary);
					}
				}
				else
				{
					if (unit.getWeapon(isPrimary).radius > 0
							|| (unit.canDamage(getTile(pos), isPrimary) && 
									getTile(pos).getUnit() != null &&
									unit.getTeam() != getTile(pos).getUnit().getTeam()))
					{
						targets.add(pos);
					}

					prevMoves.add(pos);
					getTargets(unit, newRangeLeft, pos, prevMoves, targets,
							isPrimary);
				}
			}
		}

		// LEFT
		if (col - 1 >= 0)
		{

			if (rangeLeft - 1 >= 0)
			{
				Position pos = new Position();
				pos.col = col - 1;
				pos.row = row;
				int newRangeLeft = rangeLeft - 1;
				pos.movesLeft = newRangeLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getTargets(unit, newRangeLeft, pos, prevMoves, targets,
								isPrimary);
					}
				}
				else
				{
					if (unit.getWeapon(isPrimary).radius > 0
							|| (unit.canDamage(getTile(pos), isPrimary) && 
									getTile(pos).getUnit() != null &&
									unit.getTeam() != getTile(pos).getUnit().getTeam()))
					{
						targets.add(pos);
					}

					prevMoves.add(pos);
					getTargets(unit, newRangeLeft, pos, prevMoves, targets,
							isPrimary);
				}
			}
		}

		// UP

		if (row + 1 < Constants.height)
		{

			if (rangeLeft - 1 >= 0)
			{
				Position pos = new Position();
				pos.col = col;
				pos.row = row + 1;
				int newRangeLeft = rangeLeft - 1;
				pos.movesLeft = newRangeLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getTargets(unit, newRangeLeft, pos, prevMoves, targets,
								isPrimary);
					}
				}
				else
				{
					if (unit.getWeapon(isPrimary).radius > 0
							|| (unit.canDamage(getTile(pos), isPrimary) && 
									getTile(pos).getUnit() != null &&
									unit.getTeam() != getTile(pos).getUnit().getTeam()))
					{
						targets.add(pos);
					}

					prevMoves.add(pos);
					getTargets(unit, newRangeLeft, pos, prevMoves, targets,
							isPrimary);
				}
			}
		}

		// DOWN
		if (row - 1 >= 0)
		{

			if (rangeLeft - 1 >= 0)
			{
				Position pos = new Position();
				pos.col = col;
				pos.row = row - 1;
				int newRangeLeft = rangeLeft - 1;
				pos.movesLeft = newRangeLeft;
				int index = prevMoves.indexOf(pos);
				if (index != -1)
				{
					Position oldPos = prevMoves.get(index);
					if (pos.movesLeft > oldPos.movesLeft)
					{
						prevMoves.remove(oldPos);
						prevMoves.add(pos);
						getTargets(unit, newRangeLeft, pos, prevMoves, targets,
								isPrimary);
					}
				}
				else
				{
					if (unit.getWeapon(isPrimary).radius > 0
							|| (unit.canDamage(getTile(pos), isPrimary) && 
									getTile(pos).getUnit() != null &&
									unit.getTeam() != getTile(pos).getUnit().getTeam()))
					{
						targets.add(pos);
					}

					prevMoves.add(pos);
					getTargets(unit, newRangeLeft, pos, prevMoves, targets,
							isPrimary);
				}
			}
		}
	}*/

	public Tile getTile(Position pos)
	{
		return board[pos.row][pos.col];
	}

	public static int getDefense(Unit u)
	{

		return board[u.row][u.col].getDefense();
	}

	// public getters

	public int getCurrentTeam()
	{
		return turn;
	}

	public Player getPlayer(int team)
	{
		return players[team];
	}

	public Player getCurrentPlayer()
	{
		return players[getCurrentTeam()];
	}

	public int getGold(int player)
	{
		return players[player].getGold();
	}

	public void purchase(Unit u, Tile t)
	{
		Player player = getCurrentPlayer();
		if (player.pay(UnitFactory.getCost(u)))
		{
			Unit unit = UnitFactory.createUnit(u, t.row, t.col,
					player.getTeam());
			t.setUnit(unit);

			// Update all unit state.
			ArrayList<Unit> units = allUnits.get(turn);
			units.add(unit);

			notifyObservers(t.getPosition());
			notifyObservers(player);
		}
	}

	// ---- OBSERVER STUFF

	public void notifyObservers(Object updates)
	{
		for (Observer obs : observers)
		{
			obs.update(updates);
		}

		try
		{
			Thread.sleep(100);
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void suscribe(Observer obs)
	{
		observers.add(obs);
	}

	public void finishedTile(Tile tile)
	{
		Unit unit = tile.getUnit();
		if (unit.hasMoved())
		{
			unit.setHasAttacked(true);
			notifyObservers(tile.getPosition());
		}
	}

	private void update(Integer updates)
	{
		for (Observer obs : observers)
		{
			obs.update(updates);
		}
	}

	private void notifyObservers(Unit updates)
	{
		for (Observer obs : observers)
		{
			obs.update(updates);
		}

	}

	private void notifyObservers(Object objs, String reason)
	{
		for (Observer obs : observers)
		{
			obs.update(objs, reason);
		}
	}

	/**
	 * returns if valid call
	 */
	public boolean cap(Tile origin)
	{

		if (origin != null && origin.getUnit() != null
				&& origin.getUnit().canCapture() && origin.isBuilding())
		{
			// TODO real cap logic.
			Building build = (Building) (origin);
			Unit unit = origin.getUnit();
			if (build.getTeam() != unit.getTeam())
			{
				if (build.cap())
				{
					ArrayList<Building> removeList = allBuildings.get(build
							.getTeam());
					removeList.remove(build);
					build.setTeam(unit.getTeam());

					ArrayList<Building> buildList = allBuildings.get(build
							.getTeam());
					buildList.add(build);

					if (build instanceof Headquarters)
					{
						gameOver(build.getTeam());
						return false;
					}
				}
				unit.setHasAttacked(true);
				unit.setHasMoved(true);
				notifyObservers(origin.getPosition());
				return true;
			}

		}
		System.out.println("BAD CAP CALL.");
		return false;
		// otherwise. bad call
	}

	private void gameOver(int i)
	{
		gameOver = true;
		update(i);
		System.out.println("Game has ended. Player " + i + " has won.");
		// Player i wins.
		// notifyObservers();
	}

	public ArrayList<Unit> getUnitsForTeam(int team)
	{
		return allUnits.get(team);
	}

	public ArrayList<Building> getBuildingsForTeam(int team)
	{
		return allBuildings.get(team);
	}

	public Tile getTile(Unit unit)
	{
		Position pos = unit.getPosition();
		return board[pos.row][pos.col];
	}

	public Unit getNextActiveUnit(int team)
	{
		ArrayList<Unit> units = allUnits.get(team);
		for (Unit unit : units)
		{
			if (unit.canMove())
				return unit;
		}
		return null;
	}

	public void sortUnitsByDistanceFromHq(int team)
	{
		ArrayList<Unit> units = allUnits.get(team);

		ArrayList<Building> build = allBuildings.get(team);
		Collections.sort(units, UNIT_DISTANCE_FROM_HQ_SORT);
	}

	public void sortUnitsByBeginAbleToCap(int team)
	{
		ArrayList<Unit> units = allUnits.get(team);

		ArrayList<Building> build = allBuildings.get(team);
		Collections.sort(units, UNIT_CAP_ABILITY_SORT);
	}

	public void stopUnit(Unit unit)
	{

		unit.move();
	}

	private static final Comparator<Unit> UNIT_DISTANCE_FROM_HQ_SORT = new Comparator<Unit>()
	{

		@Override
		public int compare(Unit u1, Unit u2)
		{
			int distance1 = Position.getDistance(u1.getPosition(),
					hqs[u1.getTeam()].getPosition());
			int distance2 = Position.getDistance(u2.getPosition(),
					hqs[u2.getTeam()].getPosition());
			if (distance1 > distance2)
			{
				return 1;
			}
			else if (distance1 == distance2)
			{
				return 0;
			}
			else
			{
				return -1;
			}
		}

	};

	// Cappers in front.
	private static final Comparator<Unit> UNIT_CAP_ABILITY_SORT = new Comparator<Unit>()
	{

		@Override
		public int compare(Unit u1, Unit u2)
		{
			if (u1.canCapture() && !u2.canCapture())
			{
				return -1;
			}
			else if (u1.canCapture() == u2.canCapture())
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}

	};

	public boolean isHumanPlayerTurn()
	{
		Player player = getCurrentPlayer();
		if (player instanceof Computer)
		{
			return false;
		}
		return true;
	}

	public Position getOtherTeamHQPosition(int team)
	{
		Building build = hqs[team ^ 1];
		return build.getPosition();
	}

	public int getDay()
	{
		return day;
	}
}
