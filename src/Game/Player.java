package Game;



public class Player
{

	int gold;
	int team;
	double costFactor = 1.0;

	public Player(int team)
	{
		gold = Constants.STARTING_GOLD;
		this.team = team;
	}

	public void addGenerationGold(int amount)
	{
		gold += amount;
	}

	public int getGold()
	{
		return gold;
	}

	public boolean pay(int cost)
	{
		gold -= cost;
		if (gold < 0)
		{
			System.out.println("OMFG ERROR NEGATIVE GOLD.");
			gold += cost;
			return false;
		}
		return true;
	}

	public int getTeam()
	{
		return team;
	}
}
