package Game;

public class Constants
{
	public static final int width = 15;
	public static final int height = 15;
	//http://stackoverflow.com/questions/1464291/how-to-really-read-text-file-from-classpath-in-java
	public static final String boardFile = "board.txt";
	public static final String damageChartFile = "damage.txt";
	public static final String unitFile = "unit.json";
	public static final int TEAM_RED = 1;
	public static final int TEAM_BLUE = 0;
	public static final int TEAM_NEUTRAL = 2;
	public static final int STARTING_GOLD = 50;
	public static final int GOLD_PER_BUILDING = 10;
	public static final int BUILDING_MAX_HEALTH = 100; // TODO
	public static final double UNIT_REGEN_ON_BUIDING_PERCENTAGE = 0.2;
	public static final double REPAIR_COST_FACTOR = UNIT_REGEN_ON_BUIDING_PERCENTAGE;
	public static final int BUILDING_HEALTH_REGEN = BUILDING_MAX_HEALTH;
	//public static final int TILEVIEW_SIZE = 60;
	//public static final int UNIT_MAX_HEALTH = 100;
	public static final int LUCK_MIN = -3;
	public static final int LUCK_MAX = 3;
	public static final int TILE_WIDTH_SIZE = 40;
	public static final int TILE_HEIGHT_SIZE = 40;
	public static final int INFINITE = 99999;
}
