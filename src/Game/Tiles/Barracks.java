package Game.Tiles;

public class Barracks extends Building
{
	protected Barracks(int row, int col)
	{
		super(row, col);
	}

	@Override
	public String toString()
	{
		return "BB" + getTeam();
	}
	
	
}
