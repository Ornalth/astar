package Game.Tiles;

public class Road extends Tile
{
	protected Road(int row, int col)
	{
		super(row, col);
	}

	@Override
	public String toString()
	{
		return "ROA";
	}
	
	@Override
	protected int getFootUnitCost()
	{
		return 1;
	}

	@Override
	protected int getTireUnitCost()
	{
		return 1;
	}

	@Override
	protected int getTreadUnitCost()
	{
		return 1;
	}

	@Override
	protected int getSeaUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getAirUnitCost()
	{
		return 1;
	}
	
	@Override
	public int getDefense()
	{
		return 0;
	}
}
