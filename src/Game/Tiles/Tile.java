package Game.Tiles;

import Game.Position;
import Game.Units.AirUnit;
import Game.Units.FootUnit;
import Game.Units.SeaUnit;
import Game.Units.TireUnit;
import Game.Units.TreadUnit;
import Game.Units.Unit;
import Game.Units.Weapon.AttackObject;

public abstract class Tile
{
	Unit unit;
	public int row;
	public int col;

	protected Tile(int row, int col)
	{
		this.row = row;
		this.col = col;
	}

	public boolean isBuilding()
	{
		return false;
	}

	public int getMovementCost(Unit movingUnit)
	{
		if (movingUnit == null)
		{
			System.out.println("ERROR IN GETMOVEMENTCOST MOVING UNIT IS NULL");
			return 99999;
		}

		if (unit != null && movingUnit.getTeam() != unit.getTeam())
		{
			return 99999;
		}

		if (movingUnit instanceof FootUnit)
		{
			return getFootUnitCost();
		}
		else if (movingUnit instanceof TreadUnit)
		{
			return getTreadUnitCost();
		}
		else if (movingUnit instanceof TireUnit)
		{
			return getTireUnitCost();
		}
		else if (movingUnit instanceof SeaUnit)
		{
			return getSeaUnitCost();
		}
		else if (movingUnit instanceof AirUnit)
		{
			return getAirUnitCost();
		}

		System.out.println("ERROR in get cost");
		return 99999;
	}

	abstract protected int getFootUnitCost();
	abstract protected int getTireUnitCost();
	abstract protected int getTreadUnitCost();
	abstract protected int getSeaUnitCost();
	abstract protected int getAirUnitCost();

	public Unit getUnit()
	{
		return unit;
	}

	abstract public int getDefense();

	public String getDrawableName()
	{
		return TileFactory.getDrawableName(this);
	}

	public Position getPosition()
	{
		Position p = new Position();
		p.row = row;
		p.col = col;
		return p;
	}

	public AttackObject attack(Tile tile, boolean isPrimary)
	{
		//Should not happen.
		if (getUnit() == null)
		{
			return null;
		}
		return getUnit().attack(this.getPosition(), tile,isPrimary);
	}



	public boolean hasUnit()
	{
		return unit != null;
	}

	public void setUnit(Unit unit)
	{
		this.unit = unit;

	}

	public boolean canRetaliate(Unit atker)
	{
		Unit unit = getUnit();
		if (unit == null)
		{
			return false;
		}

		return getUnit().canRetaliate(atker);
	}

	//TODO much later.
	//Tile based Attack
	/*
	public AttackObject attack(Tile tile)
    {

    }*/
}
