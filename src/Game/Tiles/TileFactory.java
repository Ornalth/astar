package Game.Tiles;

public class TileFactory
{

	public static Tile tileFromChar(char c, int row, int col)
	{
		if (c == 'G')
		{
			return new Grass(row, col);
		}
		else if (c == 'S')
		{

			return new Sea(row, col);
		}

		else if (c == 'F')
		{
			return new Forest(row, col);
		}
		else if (c == 'M')
		{
			return new Mountain(row, col);
		}
		else if (c == 'R')
		{
			return new Road(row, col);
		}

		else if (c == 'h')
		{
			return new Headquarters(row, col);
		}
		else if (c == 'p')
		{
			return new Seaport(row, col);
		}
		else if (c == 'a')
		{
			return new Airport(row, col);
		}
		else if (c == 'c')
		{
			return new City(row, col);
		}
		else if (c == 'b')
		{
			return new Barracks(row, col);
		}

		System.out.println("ERROR in tilefactory!");
		return null;
	}

	public static String getDrawableName(Tile tile)
	{
		if (tile instanceof Grass)
		{
			return "grass";
		}
		else if (tile instanceof Forest)
		{
			return "forest";
		}

		else if (tile instanceof Mountain)
		{
			return "mountain";
		}
		else if (tile instanceof Road)
		{
			return "road";
		}
		else if (tile instanceof Sea)
		{
			return "sea";
		}
		//Buildings
		else if (tile instanceof City)
		{
			return "city" + ((Building) tile).getTeam();
		}
		else if (tile instanceof Airport)
		{
			return "airport" + ((Building) tile).getTeam();
		}
		else if (tile instanceof Barracks)
		{
			return "barracks" + ((Building) tile).getTeam();
		}
		else if (tile instanceof Headquarters)
		{
			return "hq" + ((Building) tile).getTeam();
		}
		else if (tile instanceof Seaport)
		{
			return "seaport" + ((Building) tile).getTeam();
		}
		System.out.println("ERROR DID NOT FIND THIS?");
		return null;
	}

}
