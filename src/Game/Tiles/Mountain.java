package Game.Tiles;

public class Mountain extends Tile
{
	protected Mountain(int row, int col)
	{
		super(row, col);
	}

	@Override
	public String toString()
	{
		return "MTN";
	}
	
	@Override
	protected int getFootUnitCost()
	{
		return 2;
	}

	@Override
	protected int getTireUnitCost()
	{
		return 4;
	}

	@Override
	protected int getTreadUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getSeaUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getAirUnitCost()
	{
		return 1;
	}
	
	@Override
	public int getDefense()
	{
		return 4;
	}
}
