package Game.Tiles;

public class Grass extends Tile
{
	protected Grass(int row, int col)
	{
		super(row, col);
	}

	@Override
	public String toString()
	{
		return "GRA";
	}
	
	@Override
	protected int getFootUnitCost()
	{
		return 1;
	}

	@Override
	protected int getTireUnitCost()
	{
		return 2;
	}

	@Override
	protected int getTreadUnitCost()
	{
		return 1;
	}

	@Override
	protected int getSeaUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getAirUnitCost()
	{
		return 1;
	}
	
	@Override
	public int getDefense()
	{
		return 1;
	}
}
