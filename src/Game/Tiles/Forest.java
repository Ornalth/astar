package Game.Tiles;

public class Forest extends Tile
{
	protected Forest(int row, int col)
	{
		super(row, col);
	}

	@Override
	public String toString()
	{
		return "FOR";
	}
	
	@Override
	protected int getFootUnitCost()
	{
		return 1;
	}

	@Override
	protected int getTireUnitCost()
	{
		return 3;
	}

	@Override
	protected int getTreadUnitCost()
	{
		return 2;
	}

	@Override
	protected int getSeaUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getAirUnitCost()
	{
		return 1;
	}
	
	@Override
	public int getDefense()
	{
		return 2;
	}
}
