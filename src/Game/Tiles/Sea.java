package Game.Tiles;

public class Sea extends Tile
{

	protected Sea(int row, int col)
	{
		super(row, col);
	}

	@Override
	public String toString()
	{
		return "SEA";
	}
	
	@Override
	protected int getFootUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getTireUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getTreadUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getSeaUnitCost()
	{
		return 1;
	}

	@Override
	protected int getAirUnitCost()
	{
		return 1;
	}
	@Override
	public int getDefense()
	{
		return 0;
	}
}
