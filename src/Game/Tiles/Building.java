package Game.Tiles;

import java.util.ArrayList;

import Game.Constants;
import Game.Units.Unit;
import Game.Units.UnitFactory;


public abstract class Building extends Tile
{


	int team;
	int health;

	protected Building(int row, int col)
	{
		super(row, col);
		this.health = Constants.BUILDING_MAX_HEALTH;
	}

	@Override
	public boolean isBuilding()
	{
		return true;
	}

	public void setTeam(int i)
	{
		team = i;

	}

	public int getTeam()
	{
		return team;
	}



	public void restoreHealth()
	{
		health += Constants.BUILDING_HEALTH_REGEN;
		if (health > Constants.BUILDING_MAX_HEALTH)
		{
			health = Constants.BUILDING_MAX_HEALTH;
		}
	}

	public int getHealth()
	{
		return health;
	}

	@Override
	protected int getFootUnitCost()
	{
		return 1;
	}

	@Override
	protected int getTireUnitCost()
	{
		return 1;
	}

	@Override
	protected int getTreadUnitCost()
	{
		return 1;
	}

	@Override
	protected int getSeaUnitCost()
	{
		return 9999;
	}

	@Override
	protected int getAirUnitCost()
	{
		return 1;
	}

	@Override
	public int getDefense()
	{
		return 3;
	}

	public ArrayList<Unit> getGenerateOptions()
	{
		return UnitFactory.generateList(this);
	}

	public boolean hasGenerateAbility()
	{
		return UnitFactory.canGenerate(this);
	}
	
	public boolean canGenerate()
	{
		return UnitFactory.canGenerate(this) && this.unit == null;
	}

	public boolean cap()
	{

		health = (int) (health - getUnit().getCapSpeed() * getUnit().getHealth());
		if(health <= 0)
		{
			health = Constants.BUILDING_MAX_HEALTH;
			//this.team = getUnit().getTeam();
			return true;
		}
		return false;

	}


}
