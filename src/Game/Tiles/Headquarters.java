package Game.Tiles;

public class Headquarters extends Building
{
	protected Headquarters(int row, int col)
	{
		super(row, col);
	}
	@Override
	public String toString()
	{
		return "BH" + getTeam();
	}
	@Override
	public int getDefense()
	{
		return 4;
	}
}
