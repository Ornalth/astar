package Game.Tiles;

public class Seaport extends Building
{
	protected Seaport(int row, int col)
	{
		super(row, col);
	}

	@Override
	public String toString()
	{
		return "BP" + getTeam();
	}

	@Override
	protected int getSeaUnitCost()
	{
		return 1;
	}

	@Override
	public int getDefense()
	{
		return 3;
	}
}
