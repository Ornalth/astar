package Game;

public interface Observer
{
	public void update(Object obj, String reason);
	public void update(Object obj);
	public void update();
}
