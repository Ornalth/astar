package Game.Units.Weapon;

import Game.Constants;


public class Weapon
{
	public String name = "Nothing";
	public int radius = 0;
	public int baseDamage = 0;
	public DamageType type = DamageType.EXPLOSIVE;
	public int minRange = 0;
	public int maxRange = 0;
	public int maxAmmo = 0;
	public int ammo = maxAmmo;
	public boolean retal = false;



	public Weapon(Weapon w)
	{
		retal = w.retal;
		name = w.name;
		radius = w.radius;
		baseDamage = w.baseDamage;
		type = w.type;
		minRange = w.minRange;
		maxRange = w.maxRange;
		maxAmmo = w.maxAmmo;
		ammo = maxAmmo;

	}

	public Weapon()
	{
	}

	public void removeAmmo()
	{
		if (ammo != Constants.INFINITE)
		{
			ammo--;
		}
	}

	public void resupply()
	{
		ammo = maxAmmo;
	}

	public boolean hasAmmo()
	{
		return ammo > 0;
	}

	public boolean canRetal()
	{
		return retal;
	}

	public boolean hasInfiniteAmmo()
	{
		if ( ammo >= Constants.INFINITE)
		{
			return true;
		}
		return false;
	}

}
