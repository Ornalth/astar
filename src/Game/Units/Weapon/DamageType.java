package Game.Units.Weapon;

public enum DamageType
{
	LIGHT("LIGHT", 0), MEDIUM("MEDIUM", 1), HEAVY("HEAVY", 2), EXPLOSIVE("EXPLOSIVE" , 3);

	String name;
	int num;
	private static double multiplier[][] =  { 
							{ 1.2, 1.0, 0.6, 1.0 }, 
							{ 1.2, 1.0, 0.8, 1.0 },
							{ 0.8, 1.5, 1.2, 1.0 }, 
							{ 1.0, 1.0, 1.5, 1.0 }};
	
	//First == Attacker, Second == Defender

	public String getName()
	{
		return name;

	}

	private DamageType(String name, int num)
	{
		this.name = name;
		this.num = num;
	}

	public static DamageType typeFromString(String string)
	{
		for (DamageType type : values())
		{
			if (type.getName().equals(string))
			{
				return type;
			}
		}
		System.out.println("ERROR NOT FOUND INSIDE DAMAGE TYPE");
		return null;
	}

	public static double getDamageMultiplier(DamageType attack,
			DamageType defense)
	{
		double d = multiplier[attack.num][defense.num];
		System.out.println("Attacker: " + attack.getName() + " Defender: " + defense.getName() + " factor:" + d);
		return d;
	}
}
