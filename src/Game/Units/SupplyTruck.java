package Game.Units;

public class SupplyTruck extends TreadUnit
{

	public SupplyTruck()
	{
		super();
	}

	public SupplyTruck(int row, int col, int team)
	{
		super(row, col, team);
	}

	public SupplyTruck(SupplyTruck truck)
	{
		super(truck);

	}


	@Override
	public SupplyTruck deepCopy()
	{
		SupplyTruck truck = new SupplyTruck(this);
		return truck;
	}


}
