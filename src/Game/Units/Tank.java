package Game.Units;

public class Tank extends TreadUnit
{
	public Tank()
	{
		super();
	}

	public Tank(int row, int col, int team)
	{
		super(row,col,team);
	}

	public Tank(Tank tank)
	{
		super(tank);

	}


	@Override
	public Tank deepCopy()
	{
		Tank tank = new Tank(this);
		return tank;
	}
}
