package Game.Units;

public class Neotank extends TreadUnit
{
	public Neotank()
	{
		super();
	}

	public Neotank(int row, int col, int team)
	{
		super(row,col,team);
	}

	public Neotank(Neotank neo)
	{
		super(neo);

	}


	@Override
	public Neotank deepCopy()
	{
		Neotank neo = new Neotank(this);
		return neo;
	}

}
