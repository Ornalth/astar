package Game.Units;

public class Mech extends FootUnit
{

	public Mech()
	{
		super();
	}

	public Mech(int row, int col, int team)
	{
		super(row, col, team);
	}


	public Mech(Mech mech)
	{
		super(mech);

	}


	@Override
	public Mech deepCopy()
	{
		Mech mech = new Mech(this);
		return mech;
	}

}
