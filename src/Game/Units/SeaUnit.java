package Game.Units;


public abstract class SeaUnit extends Unit
{

	public SeaUnit()
	{
		super();
	}

	public SeaUnit(int row, int col, int team)
	{
		super(row, col, team);
	}

	public SeaUnit(Unit unit)
	{
		super(unit);
	}

}
