package Game.Units;



public abstract class AirUnit extends Unit
{

	public AirUnit()
	{
		super();
	}
	public AirUnit(int row, int col, int team)
	{
		super(row, col, team);
	}


	public AirUnit(Unit air)
	{
		super(air);
	}

}
