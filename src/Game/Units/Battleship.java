package Game.Units;

public class Battleship extends SeaUnit
{

	public Battleship()
	{
		super();
	}

	public Battleship(int row, int col, int team)
	{
		super(row, col, team);
	}

	public Battleship(Battleship battleship)
	{
		super(battleship);
	}

	@Override
	public Unit deepCopy()
	{
		Battleship ship = new Battleship(this);
		return ship;
	}


}
