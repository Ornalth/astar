package Game.Units;

public class BattleCopter extends AirUnit
{

	public BattleCopter()
	{
		super();
	}

	public BattleCopter(int row, int col, int team)
	{
		super(row, col, team);
	}

	public BattleCopter(BattleCopter copter)
	{
		super(copter);

	}


	@Override
	public BattleCopter deepCopy()
	{
		BattleCopter copter = new BattleCopter(this);
		return copter;
	}

}
