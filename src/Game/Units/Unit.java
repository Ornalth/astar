package Game.Units;

import Game.Board;
import Game.Constants;
import Game.MiscUtils;
import Game.Position;
import Game.Tiles.Tile;
import Game.Units.Weapon.AttackObject;
import Game.Units.Weapon.DamageType;
import Game.Units.Weapon.Weapon;

//TODO MOVE CAPTURE TO GENERIC BOOLEAN SO ANY UNIT CAN POSSILBY CAP. (NOT ONLY FOOT)
public abstract class Unit
{
	protected int health;
	protected int maxHealth;
	protected int team;
	protected int moves;
	protected boolean canMoveAndAttack;
	protected Weapon primary = new Weapon();
	protected Weapon secondary = new Weapon();
	protected String name;
	protected int id;
	protected boolean canCapture = false;
	protected double capSpeed = 1;
	// People say to do this.. amg.. man
	// Inside game info no need to copy.
	public int row;
	public int col;
	protected boolean hasMoved;
	protected boolean hasAttacked;
	protected int kills = 0;

	protected int maxGas = 0;
	protected int gas = 0;
	protected int gasUpkeep = 0;
	protected boolean dieOnGasless = false;
	protected DamageType armor;

	public Unit()
	{

	}

	public Unit(int row, int col, int team)
	{
		this.team = team;
		hasMoved = true;
		hasAttacked = true;
	}

	public Unit(Unit unit)
	{
		armor = unit.armor;
		health = unit.health;
		maxHealth = unit.maxHealth;
		team = unit.team;
		moves = unit.moves;

		canMoveAndAttack = unit.canMoveAndAttack;

		primary = new Weapon(unit.primary);
		secondary = new Weapon(unit.secondary);

		canCapture = unit.canCapture;
		capSpeed = unit.capSpeed;

		id = unit.id;
		name = unit.name;

		maxGas = unit.maxGas;
		gas = maxGas;
		gasUpkeep = unit.gasUpkeep;
		dieOnGasless = unit.dieOnGasless;

		row = unit.row;
		col = unit.col;
		kills = 0;
		hasMoved = true;
		hasAttacked = true;

	}

	abstract public Unit deepCopy();

	public void repair()
	{
		health += (Constants.UNIT_REGEN_ON_BUIDING_PERCENTAGE * maxHealth);
		if (health > maxHealth)
		{
			health = maxHealth;
		}
	}

	public void move()
	{
		if (hasMoved)
		{
			System.out.println("ERROR IN UNIT: CANNOT MOVE");
			return;
		}
		hasMoved = true;
	}

	/**
	 * 
	 * @param tile
	 * @param isPrimary
	 * @return baseDamage. & radius
	 */
	public AttackObject attack(Position selfPos, Tile tile, boolean isPrimary)
	{
		hasAttacked = true;
		AttackObject atk = new AttackObject();
		Weapon weapon = isPrimary ? primary : secondary;
		if (weapon.ammo <= 0)
		{
			atk.error = ("NO AMMO");
			return atk;
		}
		else if (weapon.radius == 0)
		{
			if (tile.getUnit() == null)
			{
				atk.error = ("MUST TARGET 0 radius");
				return atk;
			}
		}
		else
		{

			int distance = Position.getDistance(selfPos, tile.getPosition());
			if (distance > weapon.maxRange || distance < weapon.minRange)
			{
				atk.error = ("DISTANCE ERROR");
				return atk;
			}
		}
		weapon.removeAmmo();
		atk.damage = getDamage(tile, isPrimary);
		atk.type = weapon.type;
		atk.radius = weapon.radius;
		return atk;

	}

	public int getDamage(Tile tile, boolean isPrimary)
	{
		Weapon weapon = getWeapon(isPrimary);
		if (!tile.hasUnit() && weapon.radius == 0)
		{
			return 0;
		}
		
		int damage = 0;
		
		double multiplier = (double) getHealth() / (double) getMaxHealth();
		Unit unit = tile.getUnit();
		if (unit != null)
		{
			if (!(unit instanceof AirUnit))
			{
				multiplier *= (10.0 - Board.getDefense(unit)) / 10.0 ;
			}
			
			multiplier *= DamageType.getDamageMultiplier(weapon.type, unit.getArmor());
		}
		
		
		if (isPrimary)
		{
			damage = (int) (primary.baseDamage * multiplier); // TODO OTHER
			// multipliers.
		}
		else if (!isPrimary)
		{
			// int multiplier = getHealth()/Constants.UNIT_MAX_HEALTH;
			damage = (int) (secondary.baseDamage * multiplier); // TODO OTHER
			// multipliers.
		}
		
		return damage;
	}
	
	public int estimateDamageWithResistances(Tile tile, boolean isPrimary)
	{
		if (!tile.hasUnit() && getWeapon(isPrimary).radius == 0)
		{
			return 0;
		}
		
		int damage = 0;
		double multiplier = (double) getHealth() / (double) getMaxHealth();
		
		if (tile.getUnit() != null)
		{
			if (!(tile.getUnit() instanceof AirUnit))
			{
				multiplier *= (10.0 - Board.getDefense(tile.getUnit())) / 10.0 ;
			}
		}
		
		if (isPrimary)
		{
			damage = (int) (primary.baseDamage * multiplier); // TODO OTHER
			// multipliers.
		}
		else if (!isPrimary)
		{
			// int multiplier = getHealth()/Constants.UNIT_MAX_HEALTH;
			damage = (int) (secondary.baseDamage * multiplier); // TODO OTHER
			// multipliers.
		}

		return damage;
	}

	// TODO
	// max gas
	// max ammo
	// etc.
	public void resupply()
	{
		gas = maxGas;
		primary.resupply();
		secondary.resupply();
	}

	public void takeDamage(AttackObject obj, Unit source)
	{
		if (obj.damage > 0)
		{
			int dmg = obj.damage + MiscUtils.randInt(Constants.LUCK_MIN,
						Constants.LUCK_MAX);
			health -=  dmg;
					
			System.out.println(this.getName() + " took " + dmg
					+ " from " + (source != null ? source.getName() : "idk") );
		}

	}

	public void addKill(Unit unit)
	{
		kills++;
	}

	// TODO
	public AttackObject retaliate(Tile atker)
	{
		int distance = Position.getDistance(getPosition(), atker.getPosition());
		AttackObject atk = new AttackObject();

		if (primary.ammo > 0 && primary.maxRange >= distance
				&& primary.minRange <= distance && primary.radius == 0)
		{
			getPrimaryWeapon().removeAmmo();
			atk.damage = getDamage(atker, true);
			atk.type = getPrimaryWeapon().type;
			atk.radius = getPrimaryWeapon().radius;
		}
		else if (secondary.ammo > 0 && secondary.maxRange >= distance
				&& secondary.minRange <= distance && secondary.radius == 0)
		{
			getSecondaryWeapon().removeAmmo();
			atk.damage = getDamage(atker, false);
			atk.type = getSecondaryWeapon().type;
			atk.radius = getSecondaryWeapon().radius;
		}
		return atk;
	}

	/**
	 * getts/setts
	 */
	final public String getName()
	{
		return name;
	}

	public void setName(String string)
	{
		name = string;
	}

	public void setMaxHealth(int intValue)
	{
		maxHealth = intValue;
		health = maxHealth;
	}

	public void setMaxMoves(int intValue)
	{
		moves = intValue;
	}

	public void setId(int intValue)
	{
		id = intValue;
	}

	public int getId()
	{
		return id;
	}

	public int getMoves()
	{
		return moves;
	}

	public void setMoves(int moves)
	{
		this.moves = moves;
	}

	public boolean canMoveAndAttack()
	{
		return canMoveAndAttack;
	}

	public void setCanMoveAndAttack(boolean canMoveAndAttack)
	{
		this.canMoveAndAttack = canMoveAndAttack;
	}

	public boolean hasMoved()
	{
		return hasMoved;
	}

	public void setHasMoved(boolean hasMoved)
	{
		this.hasMoved = hasMoved;
	}

	public boolean hasAttacked()
	{
		return hasAttacked;
	}

	public void setHasAttacked(boolean hasAttacked)
	{
		this.hasAttacked = hasAttacked;
	}

	public void setPrimary(Weapon primary)
	{
		this.primary = primary;
	}

	public void setSecondary(Weapon secondary)
	{
		this.secondary = secondary;
	}

	public int getKills()
	{
		return kills;
	}

	public void setKills(int kills)
	{
		this.kills = kills;
	}

	public void setPrimaryWeapon(Weapon prim)
	{
		primary = prim;
	}

	public void setSecondaryWeapon(Weapon sec)
	{
		secondary = sec;
	}

	public void setHealth(int health)
	{
		this.health = health;
	}

	public int getHealth()
	{
		return health;
	}

	public int getTeam()
	{
		return team;
	}

	public void setTeam(int team)
	{
		this.team = team;
	}

	public boolean isDead()
	{
		if (gas < 0 && dieOnGasless)
			return true;
		return health <= 0;
	}

	public boolean canMove()
	{
		return !hasMoved && !hasAttacked;
	}

	public boolean canAttack()
	{
		if (!hasAttacked)
		{
			if (!hasMoved || canMoveAndAttack)
			{
				return primary.hasAmmo() || secondary.hasAmmo();
			}
		}
		return false;
	}

	public String getDrawableName()
	{
		return UnitFactory.getDrawableName(this);
	}

	public int getMaxHealth()
	{
		return maxHealth;
	}

	public boolean usesGas()
	{
		return maxGas > 0;
	}

	public int getGasUpkeep()
	{
		return 0;
	}

	public int getNumMoves()
	{
		return moves;
	}

	public Weapon getPrimaryWeapon()
	{
		return primary;
	}

	public Weapon getSecondaryWeapon()
	{
		return secondary;
	}

	public Weapon getWeapon(boolean isPrimary)
	{
		if (isPrimary)
		{
			return getPrimaryWeapon();
		}
		return getSecondaryWeapon();
	}

	public boolean canRetaliate(Unit atker)
	{

		if (atker == null || atker.getTeam() == this.getTeam())
		{
			return false;
		}
		// primary;
		int distance = Position.getDistance(getPosition(), atker.getPosition());
		if (primary.canRetal() && primary.ammo > 0
				&& primary.maxRange >= distance && primary.minRange <= distance
				&& primary.radius == 0)
		{
			return true;
		}
		else if (secondary.canRetal() && secondary.ammo > 0
				&& secondary.maxRange >= distance
				&& secondary.minRange <= distance && secondary.radius == 0)
		{
			return true;
		}

		return false;
	}

	public Position getPosition()
	{
		return new Position(this.row, this.col);
	}

	public void setCanCapture(Boolean boolean1)
	{
		canCapture = boolean1;

	}

	public boolean hasCaptureAbility()
	{
		return canCapture();
	}
	
	public boolean canCapture()
	{
		return canCapture && !hasAttacked && (!hasMoved || canMoveAndAttack);
	}

	public void renew()
	{
		hasMoved = false;
		hasAttacked = false;
	}

	public boolean getFirstWeaponChoice()
	{
		if (primary.hasAmmo())
		{
			return true;
		}
		return false;
	}

	public boolean canDamage(Tile tile, boolean isPrimary)
	{

		if (tile.getUnit() == null)
		{
			return false;
		}

		if (isPrimary && primary.ammo > 0)
		{
			return true;
		}
		else if (!isPrimary && secondary.maxAmmo > 0)
		{
			return true;
		}
		return false;
	}

	public void setCapSpeed(double capSpeed)
	{
		this.capSpeed = capSpeed;
	}

	public double getCapSpeed()
	{
		return capSpeed;
	}

	public void upkeepGas()
	{
		if (maxGas != 0)
		{
			gas -= gasUpkeep;
		}
	}

	public void payGas(int distance)
	{
		if (maxGas != 0)
		{
			gas -= distance;
		}
	}

	public void setGasUpkeep(int intValue)
	{
		gasUpkeep = intValue;
	}

	public void setMaxGas(int intValue)
	{
		maxGas = intValue;
		gas = maxGas;
	}

	public void setDieOnNoGas(Boolean boolean1)
	{
		dieOnGasless = boolean1;

	}

	public int getMaxGas()
	{
		return maxGas;
	}

	public int getGas()
	{
		return gas;
	}
	
	public double getPercentHealth()
	{
		return (double) getHealth()/(double)getMaxHealth();
	}
	
	@Override
	public String toString()
	{
		String s = this.getName() + " ";
		if ( this.getPosition() != null)
		{
			s += this.getPosition().toString();
		}
		return s;
	}

	public void setArmor(DamageType type)
	{
		armor = type;
	}

	public DamageType getArmor()
	{
		return armor;
	}
}
