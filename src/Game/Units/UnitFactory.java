package Game.Units;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import Game.Constants;
import Game.Tiles.Airport;
import Game.Tiles.Barracks;
import Game.Tiles.Building;
import Game.Tiles.Seaport;
import Game.Units.Weapon.DamageType;
import Game.Units.Weapon.Weapon;

/*
         {
            "ID": 3,
            "class": "SupplyTruck",
            "name": "Supply Truck",
            "health": 40,
            "cost": 80,
            "move": 6,
            "moveType": "TREAD",
            "MAT": true,
            "capture": false,
            "gas": 50,
            "upkeepGas": 1,
            "built": "BARRACKS"
        },
 */
public class UnitFactory
{
	private static ArrayList<Unit> allUnits = new ArrayList<Unit>();
	private static ArrayList<Unit> seaGenUnits = new ArrayList<Unit>();
	private static ArrayList<Unit> barGenUnits = new ArrayList<Unit>();
	private static ArrayList<Unit> airGenUnits = new ArrayList<Unit>();
	private static HashMap <Integer, Integer> costMap = new HashMap<Integer, Integer>();

	public static void initUnitsFromFile()
	{
		allUnits = new ArrayList<Unit>();
		seaGenUnits = new ArrayList<Unit>();
		barGenUnits = new ArrayList<Unit>();
		airGenUnits = new ArrayList<Unit>();
		costMap = new HashMap<Integer, Integer>();
		JSONParser parser = new JSONParser();
		try
		{
			InputStream stream2 = UnitFactory.class.getClassLoader().getResourceAsStream(Constants.unitFile);

			InputStreamReader s2 = new InputStreamReader(stream2);
			//new FileReader(Constants.LIBRARY_NAME)
			Object obj = parser.parse(s2);

			JSONObject mainObj = (JSONObject) obj;

			JSONArray array = (JSONArray) mainObj.get("units");
			for (int unitNumber = 0; unitNumber < array.size(); unitNumber++)
			{
				JSONObject unitObj = (JSONObject) array.get(unitNumber);
				String clsName = (String)unitObj.get("class");

				//Holy crap the hack... dayum.
				Class<?> cls = Class.forName("Game.Units." + clsName);
				Unit unit = (Unit) cls.newInstance();
				unit.setName((String)unitObj.get("name"));
				unit.setMaxHealth(((Long)unitObj.get("health")).intValue());
				unit.setMaxMoves(((Long)unitObj.get("move")).intValue());
				unit.setId(((Long)unitObj.get("ID")).intValue());
				unit.setArmor(DamageType.typeFromString((String) unitObj.get("armor")));
				if (costMap.containsKey(unit.getId()))
				{
					System.out.println("ERROR NON UNIQUE IDS");
				}
				costMap.put(unit.getId(), ((Long)unitObj.get("cost")).intValue());

				//Optionals

				if (unitObj.containsKey("capture"))
				{
					unit.setCanCapture((Boolean) unitObj.get("capture"));
				}
				if (unitObj.containsKey("captureRate"))
				{
					unit.setCapSpeed((Double) unitObj.get("captureRate"));
				}

				if (unitObj.containsKey("MAT"))
				{
					unit.setCanMoveAndAttack((Boolean) unitObj.get("MAT"));
				}

				if (unitObj.containsKey("gas"))
				{
					unit.setMaxGas(((Long)unitObj.get("gas")).intValue());
				}

				if (unitObj.containsKey("gasUpkeep"))
				{
					unit.setGasUpkeep(((Long)unitObj.get("gasUpkeep")).intValue());
				}

				if (unitObj.containsKey("dieNoGas"))
				{
					unit.setDieOnNoGas((Boolean) unitObj.get("dieNoGas"));
				}



				if (unitObj.containsKey("primary"))
				{
					JSONObject weaponObj = (JSONObject)unitObj.get("primary");
					Weapon primary = parseWeapon(weaponObj);
					unit.setPrimary(primary);
				}

				if (unitObj.containsKey("secondary"))
				{
					JSONObject weaponObj = (JSONObject)unitObj.get("secondary");
					Weapon secondary = parseWeapon(weaponObj);
					unit.setSecondary(secondary);
				}

				if (unitObj.containsKey("built"))
				{
					String builtFrom = (String)unitObj.get("built");
					if (builtFrom.equals("BARRACKS"))
					{
						barGenUnits.add(unit);
					}
					else if (builtFrom.equals("AIRPORT"))
					{
						airGenUnits.add(unit);
					}
					else if (builtFrom.equals("SEAPORT"))
					{
						seaGenUnits.add(unit);
					}
					else
					{
						System.out.println("ERROR IN BUILT FROM PARSE!");
					}
				}
				allUnits.add(unit);
				/*WEAPON--
				String name;
				int radius = 0;
			    int baseDamage = 5;
			    WeaponType type = WeaponType.BULLETS;
			    int minRange = 1;
				int maxRange = 1;
				int maxAmmo = Constants.INFINITE;
			    int ammo = maxAmmo;
				 */


				//Optionals.

				//GAS
				//Upkeep gas
				//unit.setCanCapture(((Long)unitObj.get("ID")).intValue());
				//unit.setCanMAT((Boolean) unitObj.opt("MAT"))
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		/*
		 * Attach cost onto this. will allow for less crazy crap.
	   int health;
		MoveType moveType;
		int team;
		int moves;
		public int row;
		public int col;
		boolean canMoveAndAttack;
		boolean hasMoved;
		boolean hasAttacked;
		Weapon primary;
	    Weapon secondary;
	    boolean canRetal
	    Generate from what building?!
	    int kills = 0;
		 */

	}

	private static Weapon parseWeapon(JSONObject weaponObj)
	{
		Weapon weapon = new Weapon();
		weapon.name = (String) weaponObj.get("name");
		if (weaponObj.containsKey("radius"))
		{
			weapon.radius = (((Long)weaponObj.get("radius")).intValue());
		}
		else
		{
			weapon.radius = 0;
		}

		weapon.baseDamage = (((Long)weaponObj.get("base")).intValue());
		weapon.type = DamageType.typeFromString((String) weaponObj.get("type"));
		weapon.minRange =  (((Long)weaponObj.get("min")).intValue());
		weapon.maxRange =  (((Long)weaponObj.get("max")).intValue());
		if (weaponObj.containsKey("ammo"))
		{
			weapon.maxAmmo =  (((Long)weaponObj.get("ammo")).intValue());
			weapon.ammo =  weapon.maxAmmo;
		}else
		{
			weapon.maxAmmo = Constants.INFINITE;
			weapon.ammo = weapon.maxAmmo;
		}

		if (weaponObj.containsKey("retal"))
		{
			weapon.retal = (Boolean)weaponObj.get("retal");
		}

		return weapon;
	}

	public static int getRepairCost(Unit unit)
	{

		if (unit instanceof Infantry)
		{
			return (int) (getCost(unit) * Constants.UNIT_REGEN_ON_BUIDING_PERCENTAGE);
		}
		return 0;
	}

	// TODO remake this no char no row col or team.
	public static Unit createUnit(Unit u, int row, int col, int team)
	{
		int id = u.getId();
		for (Unit unit: allUnits)
		{
			if (id == unit.getId())
			{
				Unit newUnit = unit.deepCopy();
				newUnit.row = row;
				newUnit.col = col;
				newUnit.team = team;
				newUnit.hasAttacked = true;
				newUnit.hasMoved = true;
				return newUnit;
			}
		}
		return null;
	}

	/*
	public static char charFromUnit(Unit unit)
	{
		if (unit instanceof Infantry)
		{
			return 'I';
		}
		return 0;
	}*/

	//TODO
	public static String getDrawableName(Unit unit)
	{
		if (unit instanceof Infantry)
		{
			return "inf" + unit.getTeam();
		}
		else if (unit instanceof BattleCopter)
		{
			return "bcopter" + unit.getTeam();
		}
		else if (unit instanceof Battleship)
		{
			return "bship" + unit.getTeam();
		}
		else if (unit instanceof Tank)
		{
			return "tank" + unit.getTeam();
		}
		else if (unit instanceof Artillery)
		{
			return "artillery" + unit.getTeam();
		}
		else if (unit instanceof Neotank)
		{
			return "neo" + unit.getTeam();
		}
		return null;
	}

	public static ArrayList<Unit> generateList(Building building)
	{
		if (building instanceof Barracks)
		{
			return getBarracksUnits();
		}
		else if (building instanceof Airport)
		{
			return getAirportUnits();
		}
		else if (building instanceof Seaport)
		{
			return getSeaportUnits();
		}
		return new ArrayList<Unit>();
	}

	private static ArrayList<Unit> getSeaportUnits()
	{
		return new ArrayList<Unit>(seaGenUnits);
	}

	private static ArrayList<Unit> getAirportUnits()
	{
		return new ArrayList<Unit>(airGenUnits);
	}

	private static ArrayList<Unit> getBarracksUnits()
	{
		return new ArrayList<Unit>(barGenUnits);
	}

	public static boolean canGenerate(Building building)
	{
		if (building instanceof Barracks || building instanceof Airport
				|| building instanceof Seaport)
			return true;
		return false;
	}

	public static int getCost(Unit u)
	{
		int cost = 0;
		cost = costMap.get(u.getId());
		return cost;
	}

}
