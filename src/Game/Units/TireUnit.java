package Game.Units;

public abstract class TireUnit extends Unit
{

	public TireUnit()
	{
		super();
	}
	public TireUnit(int row, int col, int team)
	{
		super(row, col, team);
	}

	public TireUnit(Unit unit)
	{
		super(unit);
	}

}
