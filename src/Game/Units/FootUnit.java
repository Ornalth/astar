package Game.Units;


public abstract class FootUnit extends Unit
{
	public FootUnit()
	{
		super();
	}
	public FootUnit(int row, int col, int team)
	{
		super(row, col, team);
	}

	public FootUnit(FootUnit unit)
	{
		super(unit);
	}


}
