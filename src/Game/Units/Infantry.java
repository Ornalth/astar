package Game.Units;


public class Infantry extends FootUnit
{

	public Infantry()
	{
		super();
	}

	public Infantry(int row, int col, int team)
	{
		super(row, col, team);
	}

	public Infantry(Infantry inf)
	{
		super(inf);

	}


	@Override
	public Infantry deepCopy()
	{
		Infantry inf = new Infantry(this);
		return inf;
	}



}
