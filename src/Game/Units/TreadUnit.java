package Game.Units;

public abstract class TreadUnit extends Unit
{

	public TreadUnit()
	{
		super();
	}
	public TreadUnit(int row, int col, int team)
	{
		super(row, col, team);
	}

	public TreadUnit(TreadUnit unit)
	{
		super(unit);
	}

}
