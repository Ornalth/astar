package Game.Units;

public class Artillery extends TreadUnit
{

	public Artillery()
	{
		super();
	}

	public Artillery(int row, int col, int team)
	{
		super(row,col,team);
	}

	public Artillery(Artillery art)
	{
		super(art);

	}


	@Override
	public Artillery deepCopy()
	{
		Artillery art = new Artillery(this);
		return art;
	}


}
