package Game.AI;

import java.util.ArrayList;
import java.util.Collections;

import Game.Board;
import Game.Constants;
import Game.Position;
import Game.Tiles.Airport;
import Game.Tiles.Barracks;
import Game.Tiles.Building;
import Game.Tiles.City;
import Game.Tiles.Headquarters;
import Game.Tiles.Seaport;
import Game.Tiles.Tile;
import Game.Units.AirUnit;
import Game.Units.Unit;
import Game.Units.UnitFactory;
import Game.Units.Weapon.Weapon;

public class InfluenceComputer extends Computer
{
	private static final int BUILDING_INFLUENCE_RADIUS = 3;

	private static final double INFLUENCE_DECAY_RATE = 0.8;

	protected static final int LOW_GAS = 20;

	protected static final double LOW_HEALTH = 20;

	protected static final int DO_NOT_MOVE_SCORE_THRESHOLD = -20;

	protected static final int CAPTURE_SCORE = 200;

	protected static final int NO_MOVE_SCORE = -999;
	protected static final int INFLUENCE_TENSION = 1000;
	private static final int INFLUENCE_FRIENDLY = 1500;
	private static final int INFLUENCE_HOSTILE = 500;

	protected static final int MOVES_FILTERED_LEFT = 10;

	protected enum InfluenceType
	{
		HOSTILE, FRIENDLY, TENSION;
	}

	public InfluenceComputer(int team)
	{
		super(team);
	}

	private Position getBestTarget(Unit unit, int[][] influenceMap,
			ArrayList<Position> possibleTargets, int[] score, boolean isPrimary)
	{
		Board board = Board.getInstance();
		int best = -999;
		Position bestTarget = null;
		Weapon weapon = unit.getWeapon(isPrimary);
		for (Position target : possibleTargets)
		{
			ArrayList<Position> damagedPositions = new ArrayList<Position>();

			damagedPositions.addAll(Position.getAllPositionsUpTo(target, 0,
					weapon.radius));

			int targetScore = 5;

			// Incase radius
			for (Position dmgTarget : damagedPositions)
			{

				Unit targettedUnit = board.getTile(dmgTarget).getUnit();

				// Score for damage.
				if (targettedUnit != null)
				{
					int multiplier = targettedUnit.getTeam() == this.getTeam() ? -1
							: 1; // Friendly fire multiplier
					int estDmg = unit.estimateDamageWithResistances(board.getTile(dmgTarget),
							isPrimary);
					int cost = UnitFactory.getCost(targettedUnit);
					int health = targettedUnit.getHealth();
					double percentDmg = (double) estDmg
							/ (double) targettedUnit.getMaxHealth();

					targetScore += percentDmg * cost / 10.0 * multiplier;

					// Can kill target!
					if (estDmg - health < 0)
					{
						targetScore += 10 * multiplier;
					}
					else if (targettedUnit.canRetaliate(unit))
					{
						targetScore -= 10;
					}

					if (targettedUnit.canCapture())
					{
						targetScore += 5 * multiplier;
					}

					Tile tile = board.getTile(targettedUnit.getPosition());
					if (tile.isBuilding())
					{
						Building build = (Building) tile;
						if (build.getTeam() == getTeam())
						{
							targetScore += 10 * multiplier;
							if (build.hasGenerateAbility())
							{
								targetScore += 30 * multiplier;
							}
						}

					}

				}

			}

			// Score for position/capture.

			if (best < targetScore)
			{
				best = targetScore;
				bestTarget = target;
			}
		}
		score[0] = best;
		return bestTarget;
	}

	/**
	 * 
	 * @param unit
	 * @param influenceMap
	 * @param possibleMoves
	 *            -- NOTE THIS INCLUDES THE ORIGINS POSITION AKA NO MOVE.
	 * @param savedBestScore
	 * @return
	 */
	private Position getBestMove(Unit unit, int[][] influenceMap,
			ArrayList<Position> possibleMoves, int[] savedBestScore)
	{
		Board board = Board.getInstance();
		int bestScore = -9999999;
		Position bestMove = null;

		for (Position move : possibleMoves)
		{
			int score = 0;
			Tile tile = board.getTile(move);
			if (tile.isBuilding())
			{
				Building build = (Building) tile;
				if (build.getTeam() != unit.getTeam())
				{
					score += 5;
					// Can cap == !!
					if (unit.canCapture())
					{
						score += 15;
						// In process of capping, DO NOT MOVE/GO THERE
						if (build.getHealth() < Constants.BUILDING_MAX_HEALTH)
						{
							score += 100;
						}
						if (build instanceof Headquarters)
						{
							score += 100;
						}
						Position p = board.getOtherTeamHQPosition(unit
								.getTeam());
						int distance = Position.getDistance(tile.getPosition(),
								p);
						if (distance < 10)
						{
							score += (20 - distance) * 5;
						}

					}
					// Block their generation
					if (build.hasGenerateAbility()
							&& build.getTeam() != Constants.TEAM_NEUTRAL)
					{
						score += 45;
					}
				}

				if (build.getTeam() == unit.getTeam())
				{
					// Want to heal our own damage units.
					if (unit.getHealth() < unit.getMaxHealth()
							* (1.0 - Constants.UNIT_REGEN_ON_BUIDING_PERCENTAGE))
					{
						score += 20;
					}
					else
					{
						score -= 10;
					}

					if (unit.usesGas() && unit.getGas() < LOW_GAS)
					{
						score += 50;
					}
					else if (!unit.getPrimaryWeapon().hasAmmo()
							|| !unit.getSecondaryWeapon().hasAmmo())
					{
						score += 30;
					}
					else
					{
						score -= 5;
					}

					// Dont want to block our own generation
					if (build.hasGenerateAbility())
					{
						score -= 150;
					}
				}
			}

			// Penalize if low on gas
			if (unit.usesGas() && unit.getGas() < LOW_GAS)
			{
				score -= 15;
			}

			if (!(unit instanceof AirUnit))
				score += tile.getDefense() * 5;

			InfluenceType type = getInfluenceType(influenceMap[move.row][move.col]);
			switch (type)
			{
				case FRIENDLY:
				{
					if (mode == Strategy.AGGRESSIVE)
					{
						score -= 50;
					}
					else if (mode == Strategy.DEFENSIVE)
					{
						score += 30;
					}
					break;
				}
				case HOSTILE:
				{
					if (mode == Strategy.AGGRESSIVE)
					{
						score += 50;
					}
					else if (mode == Strategy.DEFENSIVE)
					{
						score -= 30;
					}
					break;
				}
				case TENSION:
				{
					if (mode == Strategy.AGGRESSIVE)
					{
						score += 45;
					}
					else if (mode == Strategy.DEFENSIVE)
					{
						score += 15;
					}
					break;
				}
				default:
					break;
			}
			if (unit.canCapture())
			{
				if (type == InfluenceType.HOSTILE)
				{
					score += 20;
				}
				else if (type == InfluenceType.FRIENDLY)
				{
					score -= 20;
				}
				else if (type == InfluenceType.TENSION)
				{
					score += 10;
				}
			}
			// RANDOMIZER!
			score += rand.nextInt(5);
			if (score > bestScore)
			{
				bestMove = move;
				bestScore = score;
			}

		}
		savedBestScore[0] = bestScore;
		return bestMove;
	}

	private ArrayList<Move> evaluatePossibleMoves(Unit unit,
			int[][] influenceMap, ArrayList<Position> possibleMoves)
	{
		ArrayList<Move> moves = new ArrayList<Move>();
		int score[] = new int[1];
		for (Position pos : possibleMoves)
		{
			Move move = new Move();
			ArrayList<Position> single = new ArrayList<Position>();
			single.add(pos);
			getBestMove(unit, influenceMap, single, score);
			move.scoreMove = score[0];
			move.move = pos;
			moves.add(move);
		}
		return moves;
	}

	private void evaluateResultingActions(Unit unit, int[][] influenceMap,
			ArrayList<Move> evaluatedMoves)
	{

		Board board = Board.getInstance();
		int score[] = new int[1];
		for (Move move : evaluatedMoves)
		{
			int scoreAttackPrim = NO_MOVE_SCORE;
			int scoreAttackSec = NO_MOVE_SCORE;
			int scoreCap = NO_MOVE_SCORE;
			Position bestAttackPrimary = null;
			Position bestAttackSecondary = null;

			Tile origin = board.getTile(move.move);

			if (unit.canCapture() && origin.isBuilding()
					&& unit.getTeam() != ((Building) origin).getTeam())
			{
				scoreCap = CAPTURE_SCORE;
			}

			if (unit.getPrimaryWeapon().hasAmmo())
			{
				ArrayList<Position> possibleTargets = board
						.getSimulatedTargets(unit, origin, true);
				if (possibleTargets.size() > 0)
				{
					bestAttackPrimary = getBestTarget(unit, influenceMap,
							possibleTargets, score, true);
					scoreAttackPrim = score[0];
				}
			}

			if (unit.getSecondaryWeapon().hasAmmo())
			{
				ArrayList<Position> possibleTargets = board.getTargets(origin,
						false);
				if (possibleTargets.size() > 0)
				{
					bestAttackSecondary = getBestTarget(unit, influenceMap,
							possibleTargets, score, false);
					scoreAttackSec = score[0];
				}
			}

			int maxScore = max(scoreAttackPrim, scoreAttackSec, scoreCap);

			if (maxScore == NO_MOVE_SCORE)
			{
				move.isCap = false;
				move.scoreAction = -5; // Bad if no action!
				move.attack = null;
				move.isPrimary = true;
			}
			else if (maxScore == scoreCap)
			{
				move.isCap = true;
				move.attack = null;
				move.scoreAction = scoreCap;
			}
			else if (maxScore == scoreAttackPrim)
			{
				move.isPrimary = true;
				move.attack = bestAttackPrimary;
				move.scoreAction = scoreAttackPrim;
				move.isCap = false;
			}
			else
			{
				move.isPrimary = false;
				move.attack = bestAttackSecondary;
				move.scoreAction = scoreAttackSec;
				move.isCap = false;
			}
		}

	}

	@Override
	public void makeMove()
	{
		// Currently random;
		Board board = Board.getInstance();

		int[][] influenceMap = calcInfluence();
		board.sortUnitsByDistanceFromHq(getTeam());
		board.sortUnitsByBeginAbleToCap(getTeam());
		while (true)
		{
			Unit unit = board.getNextActiveUnit(getTeam());
			if (unit == null)
			{
				break;
			}
			// Move to random location
			Tile origin = board.getTile(unit);
			// board.cap(origin);

			if (unit.canMoveAndAttack())
			{

				ArrayList<Position> possibleMoves = board
						.getPossibleMoves(board.getTile(unit));
				possibleMoves.add(unit.getPosition());

				// int score[] = new int[1];
				// bestMove = getBestMove(unit, influenceMap, possibleMoves,
				// score);
				ArrayList<Move> evaluatedMoves = evaluatePossibleMoves(unit,
						influenceMap, possibleMoves);
				// evaluatedMoves = filterMovesDownTo(evaluatedMoves,
				// MOVES_FILTERED_LEFT);

				evaluateResultingActions(unit, influenceMap, evaluatedMoves);

				// boolean canCapOrigin = unit.canCapture() &&
				// origin.isBuilding()
				// && ((Building) origin).getTeam() != unit.getTeam();
				Move theBestMove = findBestMove(evaluatedMoves);
				executeBestMove(unit, theBestMove);

			}
			else
			{
				int scoreAttackPrim = -999;
				int scoreAttackSec = -999;
				int scoreMoveOnly = -999;
				Position bestMove = null;
				Position bestAttackPrimary = null;
				Position bestAttackSecondary = null;

				ArrayList<Position> possibleMoves = board
						.getPossibleMoves(board.getTile(unit));
				possibleMoves.add(unit.getPosition());
				int score[] = new int[1];
				bestMove = getBestMove(unit, influenceMap, possibleMoves, score);
				scoreMoveOnly = score[0];

				if (unit.getPrimaryWeapon().hasAmmo())
				{
					ArrayList<Position> possibleTargets = board.getTargets(
							origin, true);
					if (possibleTargets.size() > 0)
					{
						bestAttackPrimary = getBestTarget(unit, influenceMap,
								possibleTargets, score, true);
						scoreAttackPrim = score[0];
					}
				}

				if (unit.getSecondaryWeapon().hasAmmo())
				{
					ArrayList<Position> possibleTargets = board.getTargets(
							origin, false);
					if (possibleTargets.size() > 0)
					{
						bestAttackSecondary = getBestTarget(unit, influenceMap,
								possibleTargets, score, false);
						scoreAttackSec = score[0];
					}
				}

				int captureScore = -999;
				if (unit.canCapture() && origin.isBuilding()
						&& ((Building) origin).getTeam() != unit.getTeam())
				{
					captureScore = CAPTURE_SCORE;
				}

				int best = max(scoreAttackPrim, scoreAttackSec, scoreMoveOnly,
						captureScore);
				if (best > DO_NOT_MOVE_SCORE_THRESHOLD)
				{
					if (best == scoreAttackPrim)
					{
						board.attack(origin, board.getTile(bestAttackPrimary),
								true);
					}
					else if (best == scoreAttackSec)
					{
						board.attack(origin,
								board.getTile(bestAttackSecondary), false);
					}
					else if (best == captureScore)
					{
						board.cap(origin);
					}
					else if (best == scoreMoveOnly)
					{
						board.move(origin, board.getTile(bestMove));
					}
				}
				board.stopUnit(unit);

			}
		}
		/*
		for (Unit unit : units)
		{
			
		}
		*/
		ArrayList<Building> buildings = board.getBuildingsForTeam(getTeam());

		for (Building build : buildings)
		{
			if (build.canGenerate())
			{
				ArrayList<Unit> canBuy = new ArrayList<Unit>();
				ArrayList<Unit> canGen = build.getGenerateOptions();
				for (Unit unit : canGen)
				{
					if (UnitFactory.getCost(unit) < getGold())
					{
						canBuy.add(unit);

					}
				}

				// ??? buy random unit atm;
				// if (canBuy.size() > 0)
				// board.purchase(canBuy.get(rand.nextInt(canBuy.size())),
				// build);
				if (canBuy.size() > 0)
				{
					Position pos = build.getPosition();
					int influence = influenceMap[pos.row][pos.col];
					System.out.println("Tension at building :" + influence);

					int best = -9999;
					Unit bestUnit = null;

					for (Unit unit : canBuy)
					{
						int unitStr = UnitFactory.getCost(unit) / 10;
						int result = influence - unitStr;
			
						if (Math.abs(influence) < 5)
						{
							if (unit.hasCaptureAbility())
							{
								best = result;
								bestUnit = unit;
							}
							else
							{
								if (bestUnit == null || !bestUnit.hasCaptureAbility())
								{
									best = result;
									bestUnit = unit;
								}
							}
						}
						else
						{
							//Try to get tension to zero if big disparity
							if (Math.abs(best) > Math.abs(result))
							{
								best = result;
								bestUnit = unit;
							}
						}
						
					}
					
					if (Math.abs(best) < 15)
					{
						board.purchase(bestUnit, build);
					}
					

				}
			}
		}

		board.endTurn();
	}

	private void executeBestMove(Unit unit, Move theBestMove)
	{
		System.out.println("Unit at " + unit.getPosition());
		Board board = Board.getInstance();
		if (theBestMove == null)
		{
			board.stopUnit(unit);
			return;
		}

		Tile origin = board.getTile(unit.getPosition());

		if (theBestMove.move != null)
		{
			board.move(origin, board.getTile(theBestMove.move));

		}

		// Possible movement.
		origin = board.getTile(unit.getPosition());
		if (theBestMove.attack != null)
		{
			board.attack(origin, board.getTile(theBestMove.attack),
					theBestMove.isPrimary);
		}

		if (theBestMove.isCap)
		{
			board.cap(origin);

		}
		board.stopUnit(unit);
	}

	private int max(int... scores)
	{
		if (scores.length == 0)
		{
			return -99999999;
		}
		else
		{
			int best = scores[0];
			for (int i = 1; i < scores.length; i++)
			{
				if (best < scores[i])
				{
					best = scores[i];
				}
			}
			return best;
		}
	}

	/**
	 * CALC INFLUENCE
	 */

	private int[][] calcInfluence()
	{
		final long startTime = System.currentTimeMillis();
		Board board = Board.getInstance();
		int[][] allyInfluence = new int[Constants.height][Constants.width];
		int[][] enemyInfluence = new int[Constants.height][Constants.width];
		int[][] totalInfluence = new int[Constants.height][Constants.width];
		int enemyGold = board.getPlayer(getTeam() ^ 1).getGold();
		int allyGold = board.getPlayer(getTeam()).getGold();
		ArrayList<Unit> enemies = board.getUnitsForTeam(getTeam() ^ 1); // todo
		// fix
		ArrayList<Unit> allies = board.getUnitsForTeam(getTeam());
		ArrayList<Building> enemyBuildings = board
				.getBuildingsForTeam(getTeam() ^ 1);
		ArrayList<Building> allyBuildings = board
				.getBuildingsForTeam(getTeam());

		for (Unit enemy : enemies)
		{
			getInfluence(enemyInfluence, enemy);
		}
/*
		for (Building building : enemyBuildings)
		{
			getInfluence(enemyInfluence, building, enemyGold);
		}
*/
		for (Unit ally : allies)
		{
			getInfluence(allyInfluence, ally);
		}
/*
		for (Building building : allyBuildings)
		{
			getInfluence(allyInfluence, building, allyGold);
		}
*/
		for (int row = 0; row < Constants.height; row++)
		{
			for (int col = 0; col < Constants.width; col++)
			{
				totalInfluence[row][col] = allyInfluence[row][col]
						+ enemyInfluence[row][col];
			}
		}

		final long endTime = System.currentTimeMillis();
		System.out.println("Total execution time for influence: "
				+ (endTime - startTime));
		return totalInfluence;

	}

	// TODO CHANGE BASED ON WHAT KIND OF UNITS OPPS HAVE AND WHAT I HAVE (EX.
	// alotta air? go towards nonAIR)
	private void getInfluence(int[][] influenceMap, Building building, int gold)
	{
		Position origin = building.getPosition();
		int influence;
		if (building instanceof Airport)
		{
			influence = 50;
			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row][pos.col] += influence
							* Math.pow(INFLUENCE_DECAY_RATE, distance);
				}
			}
		}
		if (building instanceof Barracks)
		{
			influence = 40;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row][pos.col] += influence
							* Math.pow(INFLUENCE_DECAY_RATE, distance);
				}
			}
		}
		if (building instanceof Seaport)
		{
			influence = 20;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row][pos.col] += influence
							* Math.pow(INFLUENCE_DECAY_RATE, distance);
				}
			}
		}

		if (building instanceof City)
		{
			influence = 10;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row][pos.col] += influence
							* Math.pow(INFLUENCE_DECAY_RATE, distance);
				}
			}
		}

		if (building instanceof Headquarters)
		{
			influence = 75;

			for (int distance = 0; distance <= BUILDING_INFLUENCE_RADIUS; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row][pos.col] += influence
							* Math.pow(INFLUENCE_DECAY_RATE, distance);
				}
			}
		}
	}

	// TODO add in damage. && self square.
	private void getInfluence(int[][] influenceMap, Unit unit)
	{

		int sqInfluence = unit.getHealth();
		int numMoves = unit.getMoves();

		Position origin = unit.getPosition();
		boolean canCap = unit.canCapture();
		double percentHealth = unit.getHealth() / unit.getMaxHealth();
		int influence = (int) (percentHealth * UnitFactory.getCost(unit)) / 10;
		for (Position pos : Position.getAllPositionsUpTo(origin, 0, 3))
		{
			influenceMap[pos.row][pos.col] += influence
					* Math.pow(INFLUENCE_DECAY_RATE,
							Position.getDistance(origin, pos));
		}
		// also if can retal!

		// Influence from weapon/damage
		/*
		Weapon primary = unit.getPrimaryWeapon();
		int dmgInfluence = primary.hasInfiniteAmmo() ? primary.baseDamage * 5
				: primary.baseDamage * primary.ammo / 3;
		dmgInfluence /= 10;
		Weapon secondary = unit.getSecondaryWeapon();
		int dmgInfluence2 = secondary.hasInfiniteAmmo() ? secondary.baseDamage * 5
				: secondary.baseDamage * secondary.ammo / 3;
		dmgInfluence2 /= 10;

		if (dmgInfluence > 0)
		{
			for (int distance = primary.minRange; distance <= primary.maxRange; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row][pos.col] += dmgInfluence;
				}
			}

			if (unit.canMoveAndAttack())
			{
				for (int distance = primary.maxRange; distance <= primary.maxRange
						+ numMoves; distance++)
				{
					for (Position pos : Position.getAllPositionsXSquaresAway(
							origin, distance))
					{
						// TODO better factor.
						influenceMap[pos.row][pos.col] += dmgInfluence
								* Math.pow(INFLUENCE_DECAY_RATE,
										(distance - primary.maxRange));
					}
				}
			}

		}

		if (dmgInfluence2 > 0)
		{
			for (int distance = primary.minRange; distance <= primary.maxRange; distance++)
			{
				for (Position pos : Position.getAllPositionsXSquaresAway(
						origin, distance))
				{
					influenceMap[pos.row][pos.col] += dmgInfluence2;
				}
			}

			if (unit.canMoveAndAttack())
			{
				for (int distance = secondary.maxRange; distance <= secondary.maxRange
						+ numMoves; distance++)
				{
					for (Position pos : Position.getAllPositionsXSquaresAway(
							origin, distance))
					{
						influenceMap[pos.row][pos.col] += dmgInfluence2
								* Math.pow(INFLUENCE_DECAY_RATE,
										(distance - secondary.maxRange));
					}
				}
			}
		}
		*/
	}

	private InfluenceType getInfluenceType(int i)
	{
		if (i <= INFLUENCE_HOSTILE)
		{
			return InfluenceType.HOSTILE;
		}

		if (i > INFLUENCE_HOSTILE && i < INFLUENCE_FRIENDLY)
		{
			return InfluenceType.TENSION;
		}

		return InfluenceType.FRIENDLY;
	}

	private Move findBestMove(ArrayList<Move> moves)
	{
		int bestScore = -99999;
		Move bestMove = null;
		// if (moves.size() == 0)
		// return new Move();

		double moveMultiplier = 1;
		double actionMultiplier = 1;
		switch (mode)
		{
			case AGGRESSIVE:
				moveMultiplier = 0.75;
				actionMultiplier = 1.25;
				break;
			case BALANCED:
				moveMultiplier = 1.25;
				actionMultiplier = 0.75;
				break;
			case DEFENSIVE:
				moveMultiplier = 0.75;
				actionMultiplier = 1.25;
				break;
			default:
				break;

		}
		for (Move move : moves)
		{
			double score = move.scoreMove * moveMultiplier + move.scoreAction
					* actionMultiplier;

			if (bestScore < score)
			{
				bestScore = (int) score;
				bestMove = move;
			}
		}

		if (bestMove.scoreAction + bestMove.scoreMove < DO_NOT_MOVE_SCORE_THRESHOLD)
		{
			return null;
		}
		return bestMove;
	}

	private ArrayList<Move> filterMovesDownTo(ArrayList<Move> moves, int left)
	{
		if (moves.size() <= left)
		{
			return moves;
		}

		Collections.sort(moves, Move.SORT_BY_MOVE_SCORE_DESC);
		ArrayList<Move> newMoves = new ArrayList<Move>(moves.subList(0, left));
		return newMoves;

	}
}
