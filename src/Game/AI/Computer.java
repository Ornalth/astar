package Game.AI;

import java.util.Random;

import Game.Player;

//http://www.checkmarkgames.com/2012/03/building-strategy-game-ai.html
//http://gameschoolgems.blogspot.ca/2009/12/influence-maps-i.html

abstract public class Computer extends Player
{



	Random rand = new Random();
	protected Strategy mode = Strategy.AGGRESSIVE;

	protected enum Strategy
	{
		AGGRESSIVE, DEFENSIVE, BALANCED;
	}

	public Computer(int team)
	{
		super(team);
	}

	abstract public void makeMove();
	

	
}
