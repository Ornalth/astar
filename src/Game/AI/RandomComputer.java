package Game.AI;

import java.util.ArrayList;

import Game.Board;
import Game.Position;
import Game.Tiles.Building;
import Game.Tiles.Tile;
import Game.Units.Unit;
import Game.Units.UnitFactory;

public class RandomComputer extends Computer
{

	public RandomComputer(int team)
	{
		super(team);
	}

	@Override
	public void makeMove()
	{
		// Currently random;
		Board board = Board.getInstance();

		ArrayList<Unit> units = board.getUnitsForTeam(getTeam());
		//int[][] influenceMap = calcInfluence();
		for (Unit unit : units)
		{
			// Move to random location
			Tile origin = board.getTile(unit);
			board.cap(origin);

			if (unit.canMove())
			{
				ArrayList<Position> moves = board.getPossibleMoves(board
						.getTile(unit));
				if (moves.size() > 0)
				{
					Position p = moves.get(rand.nextInt(moves.size()));
					// TODO logic for a specific move...

					board.move(origin, board.getTile(p));
				}
			}

			// Attack
			if (unit.canAttack())
			{
				// Check with prim
				boolean hasAttacked = false;
				if (unit.getPrimaryWeapon().hasAmmo())
				{
					ArrayList<Position> targets = board
							.getTargets(origin, true);
					if (targets.size() > 0)
					{
						Position p = targets.get(rand.nextInt(targets.size()));
						board.attack(origin, board.getTile(p), true);
						hasAttacked = true;
					}
				}

				// 2ndary
				if (!hasAttacked && unit.getSecondaryWeapon().hasAmmo())
				{
					ArrayList<Position> targets = board.getTargets(origin,
							false);
					if (targets.size() > 0)
					{
						Position p = targets.get(rand.nextInt(targets.size()));
						board.attack(origin, board.getTile(p), false);
						hasAttacked = true;
					}
				}

			}

		}

		ArrayList<Building> buildings = board.getBuildingsForTeam(getTeam());

		for (Building build : buildings)
		{
			if (build.canGenerate())
			{
				ArrayList<Unit> canGen = build.getGenerateOptions();
				for (Unit unit : canGen)
				{
					if (UnitFactory.getCost(unit) < getGold())
					{
						board.purchase(unit, build);
						break;
					}
				}
			}
		}

		board.endTurn();
	}

}
