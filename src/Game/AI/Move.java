package Game.AI;

import java.util.Comparator;

import Game.Position;

public class Move
{
	Position attack = null;
	Position move = null;
	
	int scoreMove = -99999999;
	int scoreAction = -4234934;
	
	boolean isPrimary = false;
	
	boolean isCap = false;
	//Collections.sort(paidBills, Bill.SORTER);
	public static Comparator<Move> SORT_BY_MOVE_SCORE_DESC = new Comparator<Move>()
	{
		@Override
		public int compare(Move m1, Move m2)
		{
			if (m1.scoreMove > m2.scoreMove)
			{
				return -1;
			}
			else if (m1.scoreMove == m2.scoreMove)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	};

}
